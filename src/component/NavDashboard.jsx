import React, { useContext } from "react";
import { observer } from "mobx-react";

import Nav from "react-bootstrap/Nav";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

function NavDashboard(uiStore) {
	console.log(uiStore, uiStore.tabSelection);
	return (
		<Row>
			<Col md={12}>
				<Nav variant="tabs" defaultActiveKey="activity">
					<Nav.Item id="tab-activity" onClick={() => {}}>
						<Nav.Link eventKey="activity">Activité</Nav.Link>
					</Nav.Item>
					<Nav.Item id="tab-commands" onClick={() => {}}>
						<Nav.Link eventKey="commandes">Commandes</Nav.Link>
					</Nav.Item>
					<Nav.Item id="tab-assistance" onClick={() => {}}>
						<Nav.Link eventKey="assistance">Assistance</Nav.Link>
					</Nav.Item>
					{/* <Nav.Item
						id="tab-collaboration"
						onClick={() => {
							tabSelection.collaboration = true;
						}}
					>
						<Nav.Link eventKey="link-3">Collaboration</Nav.Link>
					</Nav.Item> */}
					<Nav.Item id="tab-exercices" onClick={() => {}}>
						<Nav.Link eventKey="link-4">Exercices</Nav.Link>
					</Nav.Item>
				</Nav>
				<h1>{uiStore.tabSelection}</h1>
			</Col>
		</Row>
	);
}

export default observer(NavDashboard);
