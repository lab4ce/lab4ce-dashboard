import RootStore from "../Rootstore";
import { observer } from "mobx-react";
import { useContext } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

import GlobalParameters from "./GlobalParameters";
import NavDashboard from "./NavDashboard";

import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Modal from "react-bootstrap/Modal";

function MainDashboard() {
	const { analytics, tracesStore, uiStore } = useContext(RootStore);

	if (analytics.uiStore.expInit) {
		return (
			<div>
				<Modal>
					<Modal.Header>
						<Modal.Title>Chargement</Modal.Title>
						<Modal.Body>
							Chargement des données, merci de patienter.
						</Modal.Body>
					</Modal.Header>
				</Modal>
				<FontAwesomeIcon icon={faSpinner} />
			</div>
		);
	} else {
		return (
			<Container fluid className="main-container">
				<Row className="main-row">
					<Col md={9} className="col-100">
						<GlobalParameters
							analytics={analytics}
							uiStore={uiStore}
							tracesStore={tracesStore}
						/>
						<NavDashboard uiStore={uiStore} />
					</Col>
					<Col md={3} className="col-student-list">
						{/* <StudentSelection /> */}
					</Col>
				</Row>
			</Container>
		);
	}
}
export default observer(MainDashboard);
