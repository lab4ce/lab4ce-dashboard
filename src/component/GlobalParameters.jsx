import { observer } from "mobx-react";

import Experience from "../model/Experience";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

function GlobalParameters({ analytics, uiStore, tracesStore }) {
	const experiences = tracesStore.experiences;

	function changeExperienceSelection(value) {
		uiStore.filter.updateCurrentExperienceId(value);
	}

	return (
		<Row id="row-global-parameters">
			<Col md={5} className="col-justify-left">
				<select
					name="tp"
					id="tp-select"
					onChange={(e) => {
						changeExperienceSelection(e.target.value);
					}}
				>
					{experiences.map((exp) => {
						return (
							<option key={exp.expId} value={exp.expId}>
								{exp.expName}
							</option>
						);
					})}
				</select>
			</Col>
			<Col md={7} className="col-justify-left">
				<div id="date-component">
					<span>Du </span>
					<input
						type="date"
						id="startDate"
						name="trip-start"
						min={analytics.experienceSelection.startDate}
						max={analytics.experienceSelection.endDate}
						defaultValue={analytics.experienceSelection.startDate}
						onChange={(e) => {
							uiStore.filter.updateStartDate(
								new Date(e.target.value)
							);
						}}
					/>
					<span> au </span>
					<input
						type="date"
						id="endDae"
						name="trip-start"
						min={analytics.experienceSelection.startDate}
						max={analytics.experienceSelection.endDate}
						defaultValue={analytics.experienceSelection.endDate}
						onChange={(e) => {
							uiStore.filter.updateEndDate(
								new Date(e.target.value)
							);
						}}
					/>
					<h1>{analytics.experienceSelection.expId}</h1>
				</div>
			</Col>
		</Row>
	);
}

export default observer(GlobalParameters);
