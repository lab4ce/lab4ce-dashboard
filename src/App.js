import "./App.css";
import RootStore from "./Rootstore";
import store, { initDashboard, initExperience } from "./model/store";
import MainDashboard from "./component/MainDashboard";

function App() {
	initDashboard();
	initExperience();
	console.log(store);

	return (
		<main>
			<RootStore.Provider value={store}>
				<div className="App">
					<h1>Test de store</h1>
					<MainDashboard />
				</div>
			</RootStore.Provider>
		</main>
	);
}

export default App;
