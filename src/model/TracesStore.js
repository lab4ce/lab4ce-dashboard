import { makeAutoObservable } from "mobx";

class TracesStore {
	_users;
	_experiences;

	constructor() {
		makeAutoObservable(this);
		this._users = [];
		this._experiences = [];
	}

	get experiences() {
		return this._experiences;
	}

	get users() {
		return this._users;
	}

	addExperience(exp) {
		if (!this._experiences.find((e) => e.expId === exp.expId)) {
			this._experiences.push(exp);
		}
	}

	addUser(user) {
		if (!this._users.find((u) => u.username === user.username)) {
			this._users.push(user);
		}
	}
}

export default TracesStore;
