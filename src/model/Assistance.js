import { makeObservable, observable, computed } from "mobx";
import Trace from "./Trace";

class Assistance extends Trace {
	_assistanceDescription = null;
	_isReplicate = null; //Bool
	_helper = null;
	_helpee = null;
	_result = null;
	_requiredhelper = null;
	// _isAtuomatic;

	constructor(
		traceId,
		author,
		dateTime,
		assistanceDescription,
		isReplicate,
		helper,
		helpee,
		result,
		requiredHelper
	) {
		super(traceId, author, dateTime);
		makeObservable(this, {
			_assistanceDescription: observable,
			_isReplicate: observable,
			_helper: observable,
			_helpee: observable,
			_result: observable,
			_requiredhelper: observable,
			assistanceDescription: computed,
			isReplicate: computed,
			helper: computed,
			helpee: computed,
			result: computed,
		});
		this._assistanceDescription = assistanceDescription;
		this._isReplicate = isReplicate;
		this._helper = helper;
		this._helpee = helpee;
		this._result = result;
		this._requiredhelper = requiredHelper;
	}

	get assistanceDescription() {
		return this._assistanceDescription;
	}

	get isReplicate() {
		return this._isReplicate;
	}

	get helper() {
		return this._helper;
	}

	get helpee() {
		return this.helpee;
	}

	get result() {
		return this._result;
	}

	get requiredHelper() {
		return this._requiredhelper;
	}
}

export default Assistance;
