import Filter from "./Filter";
import { makeAutoObservable } from "mobx";

class UIStore {
	_tabSelection;
	_filter;
	_expInit = false;

	constructor() {
		makeAutoObservable(this);
		this._tabSelection = null;
		this._filter = new Filter();
	}

	get tabSelection() {
		return this._tabSelection;
	}

	get filter() {
		return this._filter;
	}

	get expInit() {
		return this._expInit;
	}

	updateExpInit(expInitBool) {
		this._expInit = expInitBool;
	}

	updateTabSelection(tabSelection) {
		this._tabSelection = tabSelection;
	}
}

export default UIStore;
