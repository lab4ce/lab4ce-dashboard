import Analytics from "./AnalyticsStore";
import RestResourceFactory from "./RestResourceFactory";
import TracesStore from "./TracesStore";
import UIStore from "./UIStore";

const tracesStore = new TracesStore();
const uiStore = new UIStore();

const store = {
	tracesStore: tracesStore,
	uiStore: uiStore,
	analytics: new Analytics(tracesStore, uiStore),
};

export function initDashboard() {
	RestResourceFactory.initDashboard(store.analytics);
}

export function initExperience() {
	RestResourceFactory.initExperience(store.analytics, "re5f5d1gr84gred1f");
}

export default store;
