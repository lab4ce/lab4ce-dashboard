import { makeAutoObservable } from "mobx";

class Experience {
	_expId = null;
	_expName = null;
	_expStartDate = null;
	_expEndDate = null;
	_users = null;
	_exercices = null;
	_commands = null;
	_assistances = null;
	_messages = null;
	_sessions = null;
	_lastTimestamp = null;

	constructor(expId, expName, _expStartDate, _expEndDate) {
		makeAutoObservable(this);
		this._expId = expId;
		this._expName = expName;
		this._expStartDate = _expStartDate;
		this._expEndDate = _expEndDate;
		this._users = [];
		this._commands = [];
		this._assistances = [];
		this._messages = [];
		this._sessions = [];
		this._lastTimestamp = null;
	}

	get expId() {
		return this._expId;
	}

	get expName() {
		return this._expName;
	}

	get expStartDate() {
		return this._expStartDate;
	}

	get expEndDate() {
		return this._expEndDate;
	}

	get users() {
		return this._users;
	}

	get exercices() {
		return this._exercices;
	}

	get commands() {
		return this._commands;
	}
	get assistances() {
		return this._assistances;
	}
	get messages() {
		return this._messages;
	}
	get sessions() {
		return this._sessions;
	}

	get lastTimestamp() {
		return this._lastTimestamp;
	}

	addUser(user) {
		if (!this._users.find((u) => u.username === user.username)) {
			this._users.push(user);
		}
	}

	addExercice(exercice) {
		if (!this._exercices.find((ex) => ex.exerciceId === exercice.traceId)) {
			this._exercices.push(exercice);
		}
	}

	addCommand(command) {
		if (!this._commands.find((c) => c.traceId === command.traceId)) {
			this._commands.push(command);
		}
	}
	addAssistance(assistance) {
		if (!this._assistances.find((a) => a.traceId === assistance.traceId)) {
			this._assistances.push(assistance);
		}
	}
	addMessage(message) {
		if (!this._messages.find((m) => m.traceId === message.traceId)) {
			this._messages.push(message);
		}
	}
	addSession(session) {
		if (!this._sessions.find((s) => s.traceId === session.traceId)) {
			this._sessions.push(session);
		}
	}
	updateLastTimestamp(timestamp) {
		this._lastTimestamp = timestamp;
	}
}

export default Experience;
