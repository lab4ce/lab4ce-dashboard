import { makeObservable, observable, computed } from "mobx";
import Trace from "./Trace";

class Message extends Trace {
	_content = null;
	_room = null;

	constructor(traceId, author, dateTime, content, room) {
		super(traceId, author, dateTime);
		makeObservable(this, {
			_content: observable,
			_room: observable,
			content: computed,
			room: computed,
		});
		this._content = content;
		this._room = room;
	}

	get content() {
		return this._content;
	}

	get room() {
		return this._room;
	}
}

export default Message;
