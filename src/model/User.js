import { makeAutoObservable } from "mobx";

class User {
	_username;
	_firstname;
	_lastname;

	constructor(username, firstname, lastname) {
		makeAutoObservable(this);
		this._username = username;
		this._firstname = firstname;
		this._lastname = lastname;
	}

	get username() {
		return this._username;
	}
	get firstname() {
		return this._firstname;
	}
	get lastname() {
		return this._lastname;
	}
}

export default User;
