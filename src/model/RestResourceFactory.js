import Experience from "./Experience";
import User from "./User";
import Command from "./Command";
import Message from "./Message";
import Assistance from "./Assistance";

class RestResourceFactory {
	static _seuil = 120; //en secondes

	static initDashboard(analytics) {
		analytics.uiStore.updateExpInit(true);
		const [username, firstname, lastname] = this.formatUser(
			"ext_amandine.ndinga"
		);
		const currentUser = new User(username, firstname, lastname);

		const experiences = [
			{
				expId: "5fr46fez51zfr",
				expName: "Réalisation d'un LAN",
				startDate: "01-03-2018",
				endDate: "28-11-2019",
			},
			{
				expId: "re5f5d1gr84gred1f",
				expName: "Programmation Shell",
				startDate: "20-11-2018",
				endDate: "08-03-2019",
			},
			{
				expId: "5dvf1q6s2ds3q",
				expName: "TP Unix",
				startDate: "25-09-2019",
				endDate: "02-02-2020",
			},
		];
		experiences.forEach((exp) => {
			const e = new Experience(
				exp.expId,
				exp.expName,
				exp.startDate,
				exp.endDate
			);
			analytics.tracesStore.addExperience(e);
		});

		return currentUser;
	}

	static async retrieveUsernames(expId) {
		const usernames = [
			{ username: "ext_Jiale.Zhou" },
			{ username: "ext_Angel.Chambeau" },
			{ username: "ext_Sophia.Titri" },
			{ username: "ext_Kemit.Carabin" },
			{ username: "ext_Salih.Mahdi" },
			{ username: "ext_Anthony.Leca" },
			{ username: "ext_Benjamin.Vaquin" },
		];
		return usernames;
	}

	static async retrieveCommands(expId) {
		const commands = [
			{
				id: "5faebc38a9e8fe05fcc06670",
				timestamp: 1605288363440,
				expId: "faf138c3163b4dceaaf39e6ddabfc246",
				expName: "Programmation Shell S1 2020",
				labId: "9f16e627b26a4a38bf4b93b170df60d6",
				rscId: "93f6588d-6163-4185-8d97-5b84002273a1",
				username: "ext_Kemit.Carabin",
				rscName: "PC",
				command: "ls-",
				args: "l",
				response: "-bash: ls- : commande introuvable",
				score: 0,
				informations: null,
				replicate: false,
			},
			{
				id: "5faebc3aa9e8fe05fcc06678",
				timestamp: 1605288365690,
				expId: "faf138c3163b4dceaaf39e6ddabfc246",
				expName: "Programmation Shell S1 2020",
				labId: "9f16e627b26a4a38bf4b93b170df60d6",
				rscId: "93f6588d-6163-4185-8d97-5b84002273a1",
				username: "ext_Anthony.Leca",
				rscName: "PC",
				command: "ls",
				args: "-l",
				response: "total 0",
				score: 1,
				informations: null,
				replicate: false,
			},
			{
				id: "5faebc79a9e8fe05fcc066c4",
				timestamp: 1605288428156,
				expId: "faf138c3163b4dceaaf39e6ddabfc246",
				expName: "Programmation Shell S1 2020",
				labId: "9f16e627b26a4a38bf4b93b170df60d6",
				rscId: "93f6588d-6163-4185-8d97-5b84002273a1",
				username: "ext_Anthony.Leca",
				rscName: "PC",
				command: "nano",
				args: "test",
				response: "",
				score: 1,
				informations: null,
				replicate: false,
			},
			{
				id: "5faebc79dffe05fcc066c4",
				timestamp: 1605288428156,
				expId: "faf138c3163b4dceaaf39e6ddabfc246",
				expName: "Programmation Shell S1 2020",
				labId: "9f16e627b26a4a38bf4b93b170df60d6",
				rscId: "93f6588d-6163-4185-8d97-5b84002273a1",
				username: "ext_Anthony.Leca",
				rscName: "PC",
				command: "nano",
				args: "tet",
				response: "",
				score: 0,
				informations: null,
				replicate: false,
			},
			{
				id: "5faebc81a9e8fe05fcc066d9",
				timestamp: 1605288436509,
				expId: "faf138c3163b4dceaaf39e6ddabfc246",
				expName: "Programmation Shell S1 2020",
				labId: "9f16e627b26a4a38bf4b93b170df60d6",
				rscId: "93f6588d-6163-4185-8d97-5b84002273a1",
				username: "ext_Jiale.Zhou",
				rscName: "PC",
				command: "ls",
				args: "-l",
				response:
					"total 4\n-rw-r--r-- 1 root root 36 nov.  13 18:03 test",
				score: 1,
				informations: null,
				replicate: true,
			},
			{
				id: "5faebc8aa9e8fe05fcc066e9",
				timestamp: 1605288445945,
				expId: "faf138c3163b4dceaaf39e6ddabfc246",
				expName: "Programmation Shell S1 2020",
				labId: "9f16e627b26a4a38bf4b93b170df60d6",
				rscId: "93f6588d-6163-4185-8d97-5b84002273a1",
				username: "ext_Angel.Chambeau",
				rscName: "PC",
				command: "chmod",
				args: "u+x test",
				response: "",
				score: 1,
				informations: null,
				replicate: false,
			},
			{
				id: "5faebc8fa9e8fe05fcc066fa",
				timestamp: 1605288450661,
				expId: "faf138c3163b4dceaaf39e6ddabfc246",
				expName: "Programmation Shell S1 2020",
				labId: "9f16e627b26a4a38bf4b93b170df60d6",
				rscId: "93f6588d-6163-4185-8d97-5b84002273a1",
				username: "ext_Sophia.Titri",
				rscName: "PC",
				command: "./test",
				args: "",
				response: "what a terminal:",
				score: 1,
				informations: null,
				replicate: false,
			},
			{
				id: "5faebc9ea9e8fe05fcc0670b",
				timestamp: 1605288465924,
				expId: "faf138c3163b4dceaaf39e6ddabfc246",
				expName: "Programmation Shell S1 2020",
				labId: "9f16e627b26a4a38bf4b93b170df60d6",
				rscId: "93f6588d-6163-4185-8d97-5b84002273a1",
				username: "ext_Salih.Mahdi",
				rscName: "PC",
				command: "nano",
				args: "test",
				response: "",
				score: 1,
				informations: null,
				replicate: false,
			},
			{
				id: "5fb19ffb69366518ac4672dd",
				timestamp: 1605478508598,
				expId: "faf138c3163b4dceaaf39e6ddabfc246",
				expName: "Programmation Shell S1 2020",
				labId: "b7602a764bea4bc2a9fef214d791e7b5",
				rscId: "97fa9073-fc82-4c3e-bf53-a03c0826c424",
				username: "ext_Benjamin.Vaquin",
				rscName: "PC",
				command: "cd",
				args: "",
				response: "",
				score: 1,
				informations: null,
				replicate: false,
			},
			{
				id: "5fb22062158c49dead487721",
				timestamp: 1605510614048,
				expId: "faf138c3163b4dceaaf39e6ddabfc246",
				expName: "Programmation Shell S1 2020",
				labId: "37c7be00f007449dbd1d5f6b9e3ec7c4",
				rscId: "71f32126-98d4-4405-ab8c-fc9467acbb5d",
				username: "ext_Benjamin.Vaquin",
				rscName: "PC",
				command: "whoami",
				args: "",
				response: "root",
				score: 1,
				informations: null,
				replicate: false,
			},
			{
				id: "5fb22130158c49dead48795d",
				timestamp: 1605510820765,
				expId: "faf138c3163b4dceaaf39e6ddabfc246",
				expName: "Programmation Shell S1 2020",
				labId: "af9242c40c6445dc9a8add548dcca97d",
				rscId: "960b429f-b92b-4375-a429-09d261036c82",
				username: "ext_Benjamin.Vaquin",
				rscName: "PC",
				command: "nano",
				args: "tp5_exo1.sh",
				response: "",
				score: 1,
				informations: null,
				replicate: false,
			},
			,
			{
				id: "5fb22365158c49dead488d69",
				timestamp: 1605511385952,
				expId: "faf138c3163b4dceaaf39e6ddabfc246",
				expName: "Programmation Shell S1 2020",
				labId: "3fa26ff865934761b09ab7c276d576cd",
				rscId: "546b021d-f5e9-40dd-b041-d26926ab8bac",
				username: "ext_Benjamin.Vaquin",
				rscName: "PC",
				command: "nano",
				args: "tp5_exo1.sh -l",
				response: "Utilisez « fg » pour revenir à nano.",
				score: 0,
				informations: null,
				replicate: false,
			},
		];
		return commands;
	}

	static async retrieveSessions(expId) {
		const sessions = [
			{
				id: "44e4fs",
				timestamp: 1546407579,
				username: "ext_Jiale.Zhou",
				action: "loggedIn",
			},
			{
				id: "84fs64",
				timestamp: 1546421098,
				username: "ext_Jiale.Zhou",
				action: "loggedOut",
			},
			{
				id: "4vsd1c",
				timestamp: 1691025807,
				username: "ext_Benjamin.Vaquin",
				action: "loggedIn",
			},
		];
		return sessions;
	}

	static async retrieveMessage(expId) {
		const messages = [
			{
				id: "44e4fs",
				timestamp: 1562667189,
				username: "ext_Jiale.Zhou",
				message: "Bonjour",
				room: "PUBLIC",
			},
			{
				id: "84fs64",
				timestamp: 1546409112,
				username: "ext_Jiale.Zhou",
				message: "Oui, j'ai tout compris, merci",
				room: "PUBLIC",
			},
			{
				id: "d4s1df",
				timestamp: 1546409412,
				username: "ext_Benjamin.Vaquin",
				message: "Bonjour, il y a quelqu'un ?",
				room: "PUBLIC",
			},
			{
				id: "4gdg51",
				timestamp: 1546409475,
				username: "ext_Sophia.Titri",
				message: "Bonjour, oui je suis là ! :)",
				room: "PUBLIC",
			},
		];
		return messages;
	}

	static async retrieveAssistance(expId) {
		const assistances = [
			{
				id: "fq8sfq5",
				timestamp: 1546407684,
				helpee: "ext_Jiale.Zhou",
				helper: "ext_Benjamin.Vaquin",
				description:
					"Je ne comprends pas comment passer l'adresse IP en DHCP",
				isReplicate: "false",
				isAutomatic: "true",
				requiredHelper: "ext_Benjamin.Vaquin",
				result: "true",
			},
			{
				id: "sqf153",
				timestamp: 1546423875,
				helpee: "ext_Jiale.Zhou",
				helper: "ext_Benjamin.Vaquin",
				description: "Le fichier exo2.php n'affiche rien",
				isReplicate: "false",
				isAutomatic: "true",
				requiredHelper: "ext_Benjamin.Vaquin",
				result: "true",
			},
			{
				id: "sqf15f1ds3",
				timestamp: 1546423881,
				helpee: "ext_Angel.Chambeau",
				helper: "ext_Benjamin.Vaquin",
				description: "Le fichier exo2.php n'affiche rien",
				isReplicate: "true",
				isAutomatic: "true",
				requiredHelper: "ext_Benjamin.Vaquin",
				result: "true",
			},
		];
		return assistances;
	}

	static async initExperience(analytics, expId) {
		const exp = analytics.tracesStore.experiences.find(
			(experience) => experience.expId === expId
		);

		try {
			const usernames = await this.retrieveUsernames(expId);

			usernames.forEach((u) => {
				let [username, firstname, lastname] = this.formatUser(
					u.username
				);
				let user = new User(username, firstname, lastname);
				analytics.tracesStore.addUser(user);
				exp.addUser(user);
			});

			const restCommands = this.retrieveCommands(expId).then(
				(commands) => {
					commands.forEach((com) => {
						const user = exp.users.find(
							(u) => u.username === com.username
						);
						const dateTrace = new Date(com.timestamp);
						if (exp.lastTimestamp < dateTrace) {
							exp.updateLastTimestamp(dateTrace);
						}
						exp.addCommand(
							new Command(
								com.id,
								user,
								dateTrace,
								com.command,
								com.labId,
								com.rscId,
								com.args,
								com.response,
								com.score,
								com.information
							)
						);
					});
					return commands;
				}
			);

			const restMessages = this.retrieveMessage(expId).then(
				(messages) => {
					messages.forEach((message) => {
						const user = exp.users.find(
							(u) => u.username === message.username
						);
						const dateTrace = new Date(message.timestamp * 1000);
						if (exp.lastTimestamp < dateTrace) {
							exp.updateLastTimestamp(dateTrace);
						}
						exp.addMessage(
							new Message(
								message.id,
								user,
								dateTrace,
								message.message,
								message.room
							)
						);
					});
					return messages;
				}
			);

			const restAssistances = this.retrieveAssistance(expId).then(
				(assistances) => {
					assistances.forEach((assistance) => {
						const user = exp.users.find(
							(u) => u.username === assistance.helpee
						);
						const dateTrace = new Date(assistance.timestamp * 1000);
						if (exp.lastTimestamp < dateTrace) {
							exp.updateLastTimestamp(dateTrace);
						}
						exp.addAssistance(
							new Assistance(
								assistance.id,
								user,
								dateTrace,
								assistance.description,
								assistance.isReplicate,
								assistance.helper,
								user,
								assistance.result,
								assistance.requiredHelper
							)
						);
					});
					return assistances;
				}
			);

			const restSessions = this.retrieveSessions(expId).then(
				(sessions) => {
					sessions.forEach((session) => {
						const user = exp.users.find(
							(u) => u.username === session.username
						);
						const dateTrace = new Date(session.timestamp * 1000);
						if (exp.lastTimestamp < dateTrace) {
							exp.updateLastTimestamp(dateTrace);
						}

						// Calcul des sessions et création des objets
					});
				}
			);

			await Promise.all([
				restCommands,
				restMessages,
				restAssistances,
			]).finally(() => {
				analytics.uiStore.updateExpInit(false);
			});
		} catch (err) {
			console.log(err);
		}
	}

	static formatUser(username) {
		const name = username
			.replace("ext_", "")
			.replace("Ext_", "")
			.replace(".", " ");
		const firstname = name.split(" ")[0];
		const lastname = name.split(" ")[1];
		return [username, firstname, lastname];
	}
}

export default RestResourceFactory;
