import { makeObservable, observable, computed } from "mobx";

class Trace {
	_traceId = null;
	_author = null;
	_dateTime = null;

	constructor(traceId, author, dateTime) {
		makeObservable(this, {
			_traceId: observable,
			_author: observable,
			_dateTime: observable,
			traceId: computed,
			author: computed,
			dateTime: computed,
		});
		this._traceId = traceId;
		this._author = author;
		this._dateTime = dateTime;
	}

	get traceId() {
		return this._traceId;
	}

	get author() {
		return this._author;
	}

	get dateTime() {
		return this._dateTime;
	}
}

export default Trace;
