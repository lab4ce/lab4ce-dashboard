import { makeAutoObservable } from "mobx";

class Exercice {
	_exerciceId;
	_exerciceName;
	_usersValidation = [];

	constructor(exerciceId, exerciceName) {
		makeAutoObservable(this);
		this._exerciceId = exerciceId;
		this._exerciceName = exerciceName;
	}

	get exerciceId() {
		return this._exerciceId;
	}

	get exerciceName() {
		return this._exerciceName;
	}

	get usersValidation() {
		return this._usersValidation;
	}

	addUserValidation(user) {
		if (!this._usersValidation.find((u) => u.username === user.username)) {
			this._usersValidation.push(user);
		}
	}
}

export default Exercice;
