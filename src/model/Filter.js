import { makeAutoObservable } from "mobx";

class Filter {
	_startDate;
	_endDate;
	_currentExperienceId;
	_selectedUsernames = [];
	_commandLabel;
	_argsLabel;

	constructor(startDate, endDate, currentExperienceId) {
		makeAutoObservable(this);
		this._startDate = startDate;
		this._endDate = endDate;
		this._currentExperienceId = currentExperienceId;
		this._commandLabel = null;
	}

	get startDate() {
		return this._startDate;
	}

	get endDate() {
		return this._endDate;
	}

	get currentExperienceId() {
		return this._currentExperienceId;
	}

	get selectedUsernames() {
		return this._selectedUsernames;
	}

	get commandLabel() {
		return this._commandLabel;
	}

	get argslabel() {
		return this._argslabel;
	}

	updateCurrentExperienceId(experienceId) {
		this._currentExperienceId = experienceId;
	}

	updateStartDate(startDate) {
		this._startDate = startDate;
	}

	updateEndDate(endDate) {
		this._endDate = endDate;
	}

	updateCommandLabel(commandLabel) {
		this._commandLabel = commandLabel;
	}

	updateArgslabel(argsLabel) {
		this._argslabel = argsLabel;
	}

	addSelectedUsername(username) {
		if (!this._selectedUsernames.find((s) => s === username)) {
			this._selectedUsernames.push(username);
		}
	}

	emptySelectedUsernamesList() {
		this._selectedUsernames = [];
	}
}

export default Filter;
