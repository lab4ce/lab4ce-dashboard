import { makeObservable, observable, computed } from "mobx";
import Trace from "./Trace";

class Session extends Trace {
	_startDateTime = null;
	_endDateTime = null;

	constructor(traceId, author, dateTime, startDateTime, endDateTime) {
		super(traceId, author, dateTime);
		makeObservable(this, {
			_startDateTime: observable,
			_endDateTime: observable,
			startDateTime: computed,
			endDateTime: computed,
			duration: computed,
		});
		this._startDateTime = startDateTime;
		this._endDateTime = endDateTime;
	}

	get startDateTime() {
		return this._startDateTime;
	}

	get endDateTime() {
		return this._endDateTime;
	}

	get duration() {
		return this._endDateTime - this._startDateTime;
	}
}

export default Session;
