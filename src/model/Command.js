import { makeObservable, observable, computed } from "mobx";
import Trace from "./Trace";

class Command extends Trace {
	_command = null;
	_labId = null;
	_rscId = null;
	_args = null;
	_response = null;
	_score = null;
	_information = null;

	constructor(
		traceId,
		author,
		dateTime,
		command,
		labId,
		rscId,
		args,
		response,
		score,
		information
	) {
		super(traceId, author, dateTime);
		makeObservable(this, {
			_command: observable,
			_labId: observable,
			_rscId: observable,
			_args: observable,
			_response: observable,
			_score: observable,
			_information: observable,
			command: computed,
			labId: computed,
			rscId: computed,
			args: computed,
			response: computed,
			score: computed,
			information: computed,
		});
		this._command = command;
		this._labId = labId;
		this._rscId = rscId;
		this._args = args;
		this._response = response;
		this._score = score;
		this._information = information;
	}

	get command() {
		return this._command;
	}

	get labId() {
		return this._labId;
	}

	get rscId() {
		return this._rscId;
	}

	get args() {
		return this._args;
	}

	get response() {
		return this._response;
	}

	get score() {
		return this._score;
	}

	get information() {
		return this._information;
	}
}

export default Command;
