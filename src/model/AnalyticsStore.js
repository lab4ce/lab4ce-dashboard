import { makeAutoObservable } from "mobx";
import UIStore from "./UIStore";
import TracesStore from "./TracesStore";

class Analytics {
	_uiStore;
	_tracesStore;

	constructor(tracesStore, uiStore) {
		makeAutoObservable(this);
		this._uiStore = uiStore;
		this._tracesStore = tracesStore;

		//Pour les tests
		this._uiStore.filter.updateCurrentExperienceId("re5f5d1gr84gred1f");
		this._uiStore.filter.addSelectedUsername("ext_Anthony.Leca");
		this._uiStore.filter.addSelectedUsername("ext_Kemit.Carabin");
		this._uiStore.filter.addSelectedUsername("ext_Benjamin.Vaquin");
		this._uiStore.filter.addSelectedUsername("ext_Jiale.Zhou");

		this._uiStore.filter.updateStartDate(new Date("02-01-2017"));
		this._uiStore.filter.updateEndDate(new Date("08-07-2021"));
		this._uiStore.filter.updateCommandLabel("ls");
		//Pour les tests
	}

	get uiStore() {
		return this._uiStore;
	}

	get tracesStore() {
		return this._tracesStore;
	}

	get leastSuccessfullCommands() {
		// Renvoie un tableau de commandes triées par nombre d'échecs par commandes,
		// avec pour chaque label de commande 2 tableaux : les arguments de réussites et les arguments d'échec
		return 0;
	}

	get hesitationRate() {
		// Renvoie un nombre décimal
		return Math.random();
	}

	get meanDurationSession() {
		// Renvoie un nombre décimal : moyenne de temps de session pour un utilisateur
		return Math.random();
	}

	get mostAskedAssistance() {
		// Renvoie un un tableau d'objet { descriptionAssistance, nbReplicate, usernames[] }
		return 0;
	}

	get satisfactionRateHelper() {
		// Renvoie un tableau composé d'un objet { label, satisfactionRate, dissatisfactionRate } d'utilisateurs qui ont aidé
		return 0;
	}

	get satisfactionRateHelpee() {
		// Renvoie un tableau composé d'un objet { label, satisfactionRate, dissatisfactionRate } d'utilisateurs qui ont été aidés
		return 0;
	}

	get numberOfAssistanceByTarget() {
		// Renvoie un tableau composé d'objets pour chaque cible (Toute la classe, le professeur, un élève) { label }
		// ayant pour enfant un objet pour chaque catégorie (réussies, pas prise en charge, renvoyées au professeur): { label, nombre }
		return 0;
	}

	get numberOfAssistanceByTarget() {
		// Renvoie la moyenne de messages envoyés par un étudiant durant une Assistance
		return Math.random;
	}

	get validationByExercices() {
		// Renvoie un tableau d'objets Exercice avec la liste des étudiants l'ayant validé
		return 0;
	}

	get validationByExercices() {
		// Renvoie un tableau d'objets Exercice avec la liste des assistances lancées durant cet exercice
		return 0;
	}

	get allCommandsName() {
		const commands = this.getTracesByFilters(
			this._currentExperience.commands
		);

		const commandsLabel = [...new Set(commands.map((c) => c.command))];

		return commandsLabel;
	}

	get infosCommand() {
		const commandLabel = this._uiStore.filter.commandLabel;
		const exp = this._tracesStore.dictionnaryExperiences.find(
			(e) => e.expId === this._uiStore.filter.currentExperienceId
		);

		let commands = exp.commands.filter((command) =>
			this._uiStore.filter.selectedUsernames.includes(
				command.author.username
			)
		);

		return commands.filter((c) => c.command === commandLabel);
	}

	get numberOfMessagesSent() {
		const messages = this.getTracesByFilters(
			this._currentExperience.messages
		);
		return messages.length;
	}

	get meanNumberOfMessagesSent() {
		return this.numberOfMessagesSent / this._selectedUsernames.size;
	}

	get meanNumberOfSessions() {
		const sessions = this.getTracesByFilters(
			this._currentExperience.sessions
		);
		return sessions.length / this._selectedUsernames.length;
	}

	get meanDurationSession() {
		const sessions = this.getTracesByFilters(
			this._currentExperience.sessions
		);

		//Faire un reduce
		return sessions;
	}

	get successRate() {
		const commandLabel = this._uiStore.filter.commandLabel;
		const commands = this.getTracesByFilters(
			this._currentExperience.commands
		).filter((command) => command.command === commandLabel);

		const tauxReussite = commands.filter(
			(command) => command.score === 1
		).length;
		const tauxEchec = commands.filter(
			(command) => command.score === 0
		).length;

		const data = [
			{
				label: "taux",
				tauxReussite: tauxReussite,
				tauxEchec: tauxEchec,
			},
		];

		return data;
	}

	get failureRateByArgs() {
		const commandLabel = this._uiStore.filter.commandLabel;
		const commands = this.getTracesByFilters(
			this._currentExperience.commands
		).filter((command) => command.command === commandLabel);

		const failureRateByArgs = commands.reduce((acc, command) => {
			const arg = command.args.split(" ")[0];
			if (command.score === 0) {
				acc[arg] = acc[arg] + 1 || 1;
			} else {
				acc[arg] = acc[arg] || 0;
			}
			return acc;
		}, {});

		return failureRateByArgs;
	}

	get meanNumberOfMessagesSentInADay() {
		const messages = this.getTracesByFilters(
			this._currentExperience.messages
		);

		const messagesInADay = messages.reduce((acc, message) => {
			acc[message.dateTime.getHours()] =
				acc[message.dateTime.getHours()] + 1 || 1;
			return acc;
		}, {});

		let data = [];
		for (let i = 0; i < 24; i++) {
			data.push({
				x: i,
				y: messagesInADay[i] / this._selectedUsernames.size || 0,
			});
		}

		return data;
	}

	get experienceSelection() {
		const experienceSelection = this._tracesStore.experiences.find(
			(exp) => exp.expId === this._uiStore.filter.currentExperienceId
		);
		console.log(experienceSelection);
		return experienceSelection;
	}

	get hesitationRate() {
		return Math.random();
	}

	get _selectedUsernames() {
		return new Set(this._uiStore.filter.selectedUsernames);
	}

	get _startDate() {
		return this._uiStore.filter.startDate;
	}

	get _endDate() {
		return this._uiStore.filter.endDate;
	}

	get _currentExperience() {
		return this._tracesStore.dictionnaryExperiences.find(
			(e) => e.expId === this._uiStore.filter.currentExperienceId
		);
	}

	getTracesByFilters(traces) {
		let tracesUsers = null;
		if (this._selectedUsernames.length < 1) {
			tracesUsers = traces;
		} else {
			tracesUsers = traces.filter((trace) =>
				this._selectedUsernames.has(trace.author.username)
			);
		}
		const tracesByFilters = tracesUsers.filter(
			(trace) =>
				this._startDate < trace.dateTime &&
				this._endDate > trace.dateTime
		);
		return tracesByFilters;
	}
}

export default Analytics;
